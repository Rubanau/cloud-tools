#!/bin/bash

set -e 
set -o pipefail
#set -x

function say() { echo "$(date) ${SAY_PREFIX}$*" >&2 ; }
function die() { say "ERROR: $*" >&2 ; exit 1; }

DEFAULT_SAY_PREFIX="[$(basename $0 )] "
SAY_PREFIX=$DEFAULT_SAY_PREFIX

function usage() {
    cat >&2 <<USAGE
Creates keypairs in AWS and stores their private parts in AWS secrets manager

Synopsis:
    $(basename $0 ) KEYNAME1 KEYNAME2 ... KEYNAMEN
USAGE
}

function key_pair_exists() {
    result=$( aws ec2 describe-key-pairs --key-names $* 2>/dev/null || true )
    ! [ -z "$result" ];
}

if [ -z "$*" ]; then usage; exit 0; fi

for KEYNAME in $* ; do

    SAY_PREFIX="$DEFAULT_SAY_PREFIX Keypair $KEYNAME: "
    if key_pair_exists $KEYNAME ; then  
        say "already exists"
        continue
    fi

    PRIVATEKEY=$(mktemp -p $PWD XXXXXX.pk)
    SECRETNAME=sshkeys/$KEYNAME
    aws ec2 create-key-pair --key-name $KEYNAME | jq -r .KeyMaterial > $PRIVATEKEY 
    aws secretsmanager create-secret --name $SECRETNAME --secret-string file://$PRIVATEKEY
    rm $PRIVATEKEY
    say "stored at $SECRETNAME"

done

SAY_PREFIX=$DEFAULT_SAY_PREFIX say "No more keys to process. Bye."
