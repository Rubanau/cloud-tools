#!/bin/bash

### Usage
# ```
# source "$( curl -s  https://gitlab.com/Rubanau/cloud-tools/raw/master/envvars_check.sh )"
# envvars_check_report VAR1 VAR2 VAR3 VAR4 VAR5
# envvars_check_warn_if_missing VAR2 VAR3 
# envvars_check_abort_if_missing VAR4 VAR5 
# ```

envvars_check_report() {
    for varname in $* ; do
        echo "${varname}=${!varname}" >&2
    done
}

envvars_check_detect_missing() {
    local missing_variables=() 
    for varname in $* ; do
        if [ -z "${!varname}" ]; then
            missing_variables+=("$varname")
        fi 
    done 
    echo "${missing_variables[*]}"
}

envvars_check_warn_if_missing() {
    missing=$( envvars_check_detect_missing $* )
    if [ ! -z "$missing" ]; then
        echo "WARNING! Following environment variables are missing: $missing" >&2
    fi
}

envvars_check_abort_if_missing() {
    missing=$( envvars_check_detect_missing $* )
    if [ ! -z "$missing" ]; then
        echo "ABORT! Following environment variables are missing: $missing" >&2
        exit 255
    fi
}


