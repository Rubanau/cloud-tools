#!/bin/bash

set -e

### example for e.g. cloud-init

### parameters for target script which would be eventually executed from fetched directory
# 
# export ANSIBLE_VAR_1="parameter 1"
# export ANSIBLE_VAR_2="parameter 2"

### parameters for fetch-and-run
# export FETCH_AND_RUN_GIT_URL="..."
# export FETCH_AND_RUN_GIT_KEY_BODY="..."
# export FETCH_AND_RUN_GIT_BRANCH="..." # optional, default is "master"
# export FETCH_AND_RUN_WEB_URL="..."
# export FETCH_AND_RUN_WEB_USER="..."
# export FETCH_AND_RUN_WEB_PASSWORD="..."
# export FETCH_AND_RUN_WEB_PASSWORD_FILE="..."
# export FETCH_AND_RUN_DIR_NAME="..."
# export FETCH_AND_RUN_SUBDIR='...' # optional
# export FETCH_AND_RUN_COMMAND
# execution
### curl -s https://gitlab.com/Rubanau/cloud-tools/raw/master/fetch_and_run.sh | /bin/bash 2>&1 | logger -e -s -t setup_server




say() { echo "$*" >&2 ; }
die() { say "ERROR: $*" ;  exit 1; }


validate_parameters() {
    if [ -n "$FETCH_AND_RUN_GIT_URL" ] && [ -n "$FETCH_AND_RUN_WEB_URL" ]; then
        die "Both FETCH_AND_RUN_GIT_URL and FETCH_AND_RUN_WEB_URL are defined!"
    fi
    if [ -z "$FETCH_AND_RUN_GIT_URL" ] && [ -z "$FETCH_AND_RUN_WEB_URL" ]; then
        die "Neither of FETCH_AND_RUN_GIT_URL or FETCH_AND_RUN_GIT_URL is defined!"
    fi

    if [ -z "$FETCH_AND_RUN_DIR_NAME" ]; then
        die "FETCH_AND_RUN_DIR_NAME not set"
    else
        say "FETCH_AND_RUN_DIR_NAME=$FETCH_AND_RUN_DIR_NAME"
    fi

    if [ -z "$FETCH_AND_RUN_SUBDIR" ]; then
        say "FETCH_AND_RUN_WORKDIR=$FETCH_AND_RUN_SUBDIR"
    fi  

    if [ -n "$FETCH_AND_RUN_GIT_URL" ]; then
        export FETCH_AND_RUN_GIT_BRANCH=${FETCH_AND_RUN_GIT_BRANCH:-master}
        say "FETCH_AND_RUN_GIT_BRANCH=${FETCH_AND_RUN_GIT_BRANCH}"
    fi

    if [ -n "$FETCH_AND_RUN_WEB_URL" ]; then
        if [ -n "$FETCH_AND_RUN_WEB_USER" ]; then
            say "FETCH_AND_RUN_WEB_USER=$FETCH_AND_RUN_WEB_USER"
            if [ -z "$FETCH_AND_RUN_WEB_PASSWORD" ] && [ -z "$FETCH_AND_RUN_WEB_PASSWORD_FILE" ]; then
                die "As FETCH_AND_RUN_WEB_USER is defined, either FETCH_AND_RUN_WEB_PASSWORD or FETCH_AND_RUN_WEB_PASSWORD_FILE must be defined as well"
            fi
            if [ -n "$FETCH_AND_RUN_WEB_PASSWORD_FILE" ]; then
                if [ -n "$FETCH_AND_RUN_WEB_PASSWORD" ]; then
                    die "Ambigous web credentials: both FETCH_AND_RUN_WEB_PASSWORD and FETCH_AND_RUN_WEB_PASSWORD_FILE are defined" 
                fi

                if [ ! -r "$FETCH_AND_RUN_WEB_PASSWORD_FILE" ]; then
                    die "File $FETCH_AND_RUN_WEB_PASSWORD_FILE does not exist or is not readable"
                fi
            fi
        fi
    fi

    if [ -n "$FETCH_AND_RUN_SUBDIR" ]; then
        say "FETCH_AND_RUN_SUBDIR=$FETCH_AND_RUN_SUBDIR"
    fi
    if [ -z "$FETCH_AND_RUN_COMMAND" ]; then
        die "No FETCH_AND_RUN_COMMAND defined"
    else
        say "FETCH_AND_RUN_COMMAND=$FETCH_AND_RUN_COMMAND"
    fi

}


check_dependencies() {

    
    if [ -z "$(which curl || true )" ] ; then
        say "curl missing, trying to install"
        apt-get update && apt-get install -y curl
    fi

    if [ -n "$FETCH_AND_RUN_GIT_URL" ]; then
        if [ -z "$(which git || true)" ]; then
            say "git missing, trying to install"
            apt-get update && apt-get install -y git
        fi

        if [ ! -e /etc/ssh/ssh_known_hosts ] && [ -z "$FETCH_AND_RUN_SKIP_GIT_PLATFORMS_KNOWN_HOSTS_TWEAK" ]; then
            say "Trying to add major providers fingerprints to the system's known hosts"
            # make ssh keys of major git platforms known to the host
            curl -s https://gitlab.com/Rubanau/cloud-tools/raw/master/ssh_validate_git_providers_fingerprints.sh \
                    | /bin/bash | tee -a /etc/ssh/ssh_known_hosts
        fi
    else # web flow
        # options:
        #   this script should NOT try to install wsid
        #   instead cloud init could first fetch-and-run wsid bootstrapper, than fetch-and-run authorized provisioner
        #   run anonymously
        #   use wsid:
        #       provision first
        #       use existing
        if [ -n "$FETCH_AND_RUN_WEB_USER" ]; then
            if [ -z "$( which base64 || true )" ]; then
                say "base64 utility missing, trying to install coreutils"
                apt-get update && apt-get install -y coreutils
            fi
        fi

    fi
}

check_home_env() {
    if [ -z "$HOME" ]; then
        if [ "$(whoami)" == "root" ]; then
            say "export HOME=/root"
            export HOME=/root
        else
            say "WARNING: HOME not set"
        fi
    fi
}

fetch_from_git() {

    say "Start: check out branch $FETCH_AND_RUN_GIT_BRANCH from $FETCH_AND_RUN_GIT_URL into $FETCH_AND_RUN_DIR_NAME"

    FETCH_AND_RUN_DIR_PARENT=$( dirname "$FETCH_AND_RUN_DIR_NAME" )
    
    OLD_GIT_SSH_COMMAND="$GIT_SSH_COMMAND"
    if [ -n "$FETCH_AND_RUN_GIT_KEY_BODY" ]; then
        TEMPORARY_KEY=$( mktemp /dev/shm/farkey.XXXXXX  )
        echo "$FETCH_AND_RUN_GIT_KEY_BODY" > $TEMPORARY_KEY 
        chmod 0600 $TEMPORARY_KEY
        export GIT_SSH_COMMAND="ssh -i $TEMPORARY_KEY"
    fi 

    if [ ! -e "$FETCH_AND_RUN_DIR_NAME" ]; then
        mkdir -pv $FETCH_AND_RUN_DIR_PARENT
        cd $FETCH_AND_RUN_DIR_PARENT 
        say "Cloning $FETCH_AND_RUN_GIT_URL to $FETCH_AND_RUN_DIR_NAME" 
        git clone "$FETCH_AND_RUN_GIT_URL" "$FETCH_AND_RUN_DIR_NAME" 
    fi 

    cd $FETCH_AND_RUN_DIR_NAME 
    DESIRED_BRANCH_LOCAL=$( git branch | egrep '^\*?\s*'$FETCH_AND_RUN_GIT_BRANCH'\s*$' || true ) 
    DESIRED_BRANCH_REMOTE=$( git branch -a | egrep '^\s*remotes/origin/'$FETCH_AND_RUN_GIT_BRANCH'\s*$' || tre ) 
    if [ -z "$DESIRED_BRANCH_LOCAL" ]; then
        if [ -z "$DESIRED_BRANCH_REMOTE" ]; then
            git fetch origin
        fi
        git checkout -t "origin/$FETCH_AND_RUN_GIT_BRANCH"
    fi
    git checkout "$FETCH_AND_RUN_GIT_BRANCH"
    git pull

    if [ -n "$FETCH_AND_RUN_GIT_KEY_BODY" ]; then
        export GIT_SSH_COMMAND="$OLD_GIT_SSH_COMMAND"
        rm -f $TEMPORARY_KEY
    fi
    
    say "Done: check out branch $FETCH_AND_RUN_GIT_BRANCH from $FETCH_AND_RUN_GIT_URL into $FETCH_AND_RUN_DIR_NAME"
}

get_curl_auth() {
    if [ -n "$FETCH_AND_RUN_WEB_USER" ]; then
        local basic_passwd="$FETCH_AND_RUN_WEB_PASSWORD"
        if [ -n "$FETCH_AND_RUN_WEB_PASSWORD_FILE" ]; then  
            basic_passwd=$( cat "$FETCH_AND_RUN_WEB_PASSWORD_FILE" )
        fi
        local auth_raw="$FETCH_AND_RUN_WEB_USER:$basic_passwd"
        local auth_encoded=$( echo -n "$auth_raw" | base64 -w0 )
        echo "-H 'Authorization: Basic $auth_encoded'" 
    fi
}

fetch_from_web() {
    local outfile=$( mktemp /dev/shm/farweb.XXXXXX.tgz )
    say "Fetching $FETCH_AND_RUN_WEB_URL"
    local auth_flags=$( get_curl_auth )
    curl -s -o "$outfile" $auth_flags "$FETCH_AND_RUN_WEB_URL" 
    
    say "Unpacking into $FETCH_AND_RUN_DIR_NAME"
    mkdir -pv "$FETCH_AND_RUN_DIR_NAME"
    tar xzvf $outfile -C "$FETCH_AND_RUN_DIR_NAME"

    say "Done: fetch $FETCH_AND_RUN_WEB_URL and unpack into $FETCH_AND_RUN_DIR_NAME"
    rm -f $outfile
}

fetch_sources() {
    if [ -n "$FETCH_AND_RUN_GIT_URL" ]; then
        fetch_from_git
    elif [ -n "$FETCH_AND_RUN_WEB_URL" ]; then
        fetch_from_web
    else
        die "Neither of FETCH_AND_RUN_GIT_URL or FETCH_AND_RUN_WEB_URL is defined"
    fi
}   

do_run() {
    cd "$FETCH_AND_RUN_DIR_NAME"
    if [ -n "$FETCH_AND_RUN_SUBDIR" ]; then
        if [ ! -e "$FETCH_AND_RUN_SUBDIR" ]; then
            die "SUBDIR $FETCH_AND_RUN_SUBDIR is missing";
        else
            cd "$FETCH_AND_RUN_SUBDIR"
        fi
    fi
    say "Running in $(pwd): $FETCH_AND_RUN_COMMAND"
    exec $FETCH_AND_RUN_COMMAND
}

FETCH_AND_RUN_DIR_NAME=${FETCH_AND_RUN_DIR_NAME:-/usr/src/fetch-and-run}
validate_parameters
check_dependencies
check_home_env
fetch_sources
do_run


