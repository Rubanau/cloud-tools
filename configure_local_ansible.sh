#!/bin/bash 

### run as :
### curl -s https://gitlab.com/Rubanau/cloud-tools/raw/master/configure_local_ansible.sh | /bin/bash

set -e
set -o pipefail

echo "Configuring ansible for localhost" >&2 

mkdir -p /etc/ansible/host_vars

cat > /etc/ansible/ansible.cfg <<ANSIBLECFG
[defaults]
inventory=/etc/ansible/hosts
ANSIBLECFG

cat > /etc/ansible/hosts <<HOSTS
[local]
localhost ansible_connection=local
HOSTS


generate_ansible_config_body() {
python -c "
import os;
newline=\"\\n\";
relevant_keys=[ k for k in sorted(os.environ.keys()) if k.startswith('ANSIBLE_VAR_') ];
relevant_lines=[ k[len('ANSIBLE_VAR_'):].lower()+': '+str(os.environ[k]) for k in relevant_keys ];
relevant_lines = (['---']+relevant_lines+[newline]) if relevant_lines else []
print(newline.join(relevant_lines));
"
}

generate_ansible_config() {
    local targetfile=${1:-/etc/ansible/host_vars/localhost}
    if [ -s "$targetfile" ]; then
        echo "$targetfile exists and is non-empty, not overwriting"
    else
        echo "Generating ansible config"
        mkdir -p "$( dirname ${targetfile} )"
        generate_ansible_config_body | tee $targetfile 
    fi
}

if [ ! -z "$( env | grep 'ANSIBLE_VAR_' || true  )" ]; then 
   generate_ansible_config 
fi 
