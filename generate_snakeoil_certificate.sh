#!/bin/bash


SNAKEOIL_COMPANY_DEPT="IT"
SNAKEOIL_COMPANY_NAME="My company"
SNAKEOIL_COMPANY_CITY="London"
SNAKEOIL_COMPANY_COUNTRY="UK"

PRIMARY_DOMAIN=$1
shift
DOMAINS_LIST=$*
if [ -z "$DOMAINS_LIST" ]; then DOMAINS_LIST=$PRIMARY_DOMAIN ; fi

SSL_CERT_KEYFILE=./snakeoil.key
SSL_CERT_PEMFILE=./snakeoil.pem

CSRTEMPLATE=$(tempfile)
cat > $CSRTEMPLATE <<CSR
[ req ]
prompt = no
default_bits = 2048
default_keyfile = privkey.pem
encrypt_key = no
distinguished_name = req_distinguished_name
 
string_mask = utf8only
 
req_extensions = v3_req,san

[ v3_req ]
basicConstraints = CA:FALSE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment

[ req_distinguished_name ]
OU=$SNAKEOIL_COMPANY_DEPT
O=$SNAKEOIL_COMPANY_NAME
L=$SNAKEOIL_COMPANY_CITY
C=$SNAKEOIL_COMPANY_COUNTRY
CN=$PRIMARY_DOMAIN

[san]
subjectAltName=$DOMAINS_LIST
CSR

openssl req -x509 -nodes -days 3650 -newkey rsa:2048 \
        -keyout $SSL_CERT_KEYFILE                    \
        -out $SSL_CERT_PEMFILE                       \
        -reqexts san -extensions san                 \
        -config $CSRTEMPLATE

rm $CSRTEMPLATE

