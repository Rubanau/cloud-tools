FROM python:3-alpine

RUN apk --no-cache add --update \
        openssh-client curl     \
        bash git terraform      \
        pwgen   jq       

RUN pip3 install awscli boto3

COPY ssh_validate_git_providers_fingerprints.sh /usr/local/bin/ssh_validate_git_providers_fingerprints.sh

RUN chmod a+x /usr/local/bin/ssh_validate_git_providers_fingerprints.sh 
RUN /usr/local/bin/ssh_validate_git_providers_fingerprints.sh >> /etc/ssh/known_keys

ARG DOCKER_CLI_VERSION=18.06.3
RUN wget -q -O /tmp/docker.tgz \
        https://download.docker.com/linux/static/stable/$(uname -m)/docker-${DOCKER_CLI_VERSION}-ce.tgz \
    && cd /tmp              \
    && tar -xzf docker.tgz  \
    && cp docker/docker /usr/local/bin/docker-cli \
    && rm -rf /tmp/docker*


