#!/bin/bash

### run as :
### curl -s https://gitlab.com/Rubanau/cloud-tools/raw/master/install_ansible_apt.sh | /bin/bash

set -e 
set -o pipefail

echo "Installing python3(apt), pip3(apt), and ansible (pip3)" >&2

say() { echo "$*" >&2 ; }

setup_pythonpath() {
        pythonpath="$(which python || true)"
        if [ -z "$pythonpath" ]; then
            python3path=$(which python3 || true)
            if [ ! -z "$python3path" ]; then
                echo "Adding python3 path $python3path to alternatives" >&2 ; 
                update-alternatives --install /usr/bin/python python $python3path 20 ;
            else 
                echo "WARNING: no python path and no python3 path found!" >&2 ;
            fi 
        else 
            echo "Python path: $pythonpath" >&2 ;
        fi

}

say "Installing python3"
apt-get update && apt-get install -y python3 python3-pip 

say "Setting up pythonpath"
setup_pythonpath

say "Installing ansible"
pip3 install ansible

