#!/bin/bash

### run as :
### curl -s https://gitlab.com/Rubanau/cloud-tools/raw/master/build_nginx_vts_module_debian.sh | /bin/bash

set -e

vts_version=0.1.18
nginx_version=$( nginx -v 2>&1 | cut -f2 -d/ )
vts_module_dir=nginx-module-vts-${vts_version}
cd /usr/src

curl -o nginx_vts_module.tar.gz -Ls https://github.com/vozlt/nginx-module-vts/archive/v${vts_version}.tar.gz
curl -o nginx.tar.gz 		-Ls https://nginx.org/download/nginx-${nginx_version}.tar.gz
tar xzf nginx.tar.gz
tar xzf nginx_vts_module.tar.gz 

BUILD_DEPS="build-essential libpcre++-dev zlib1g-dev libxslt1-dev libxml2-dev libssl-dev"
apt-get update && apt-get install -y $BUILD_DEPS 

cd nginx-${nginx_version}
nginx -V 2>&1 | grep 'configure arguments'  | cut -f2 -d: | python -c 'import sys; print(" ".join(["./configure"]+[ s for s in sys.stdin.readline().strip().split(" ") if not s.startswith("--add-dynamic-module")] + ["--add-dynamic-module=../'$vts_module_dir'/"]))'  | /bin/bash
make -j4 
cp -v objs/ngx_http_vhost_traffic_status_module.so /etc/nginx/modules/

cd /usr/src
rm -rf nginx.tar.gz nginx_vts_module.tar.gz $vts_module_dir nginx-${nginx_version}

apt-get remove -y $BUILD_DEPS
