#!/bin/bash

# curl -s https://gitlab.com/Rubanau/cloud-tools/raw/master/ssh_validate_git_providers_fingerprints.sh | /bin/bash | tee -a /etc/ssh/ssh_known_hosts
set -e

#GITHUB_FINGERPRINTS_URL="https://docs.github.com/en/github/authenticating-to-github/githubs-ssh-key-fingerprints"
GITHUB_FINGERPRINTS_URL="https://docs.github.com/en/github/authenticating-to-github/keeping-your-account-and-data-secure/githubs-ssh-key-fingerprints"
BITBUCKET_FINGERPRINTS_URL="https://support.atlassian.com/bitbucket-cloud/docs/configure-ssh-and-two-step-verification/"
GITLAB_FINGERPRINTS_URL="https://docs.gitlab.com/ee/user/gitlab_com/"

function say() { echo "[$(basename $0)] $* " >&2 ; }
function die() { say "ERROR: $*" >&2 ; }
function run() { say "RUN: $*"; $* ; }

TMPDIR=$( mktemp -d )
cd $TMPDIR

#########################################
### GITHUB 

say "Scanning github"
ssh-keyscan github.com > github.com.keys 2>/dev/null
github_fingerprint=$(ssh-keygen -l -f github.com.keys | cut -f2 -d' ')

say "Obtaining declared fingerprints from $GITHUB_FINGERPRINTS_URL"
curl -s -o github_fingerprints.html $GITHUB_FINGERPRINTS_URL

say "Comparing github fingerprints"
github_sha_lines=$( grep '<li><code>' github_fingerprints.html | cut -f3 -d '>' | cut -f 1 -d '<' )

if [ "$DEBUG"="yes" ] ; then
    say "DEBUG: github_fingerprint=$github_fingerprint" 
    say "DEBUG: github_sha_lines=$github_sha_lines"
fi
if [ !  -z "$github_fingerprint" ] && echo "$github_sha_lines" | grep -q "$github_fingerprint" ; then
    say "Github fingerprint validated"
    cat github.com.keys 
else
    say "ERROR: Validation failed for github keys!"
fi

#########################################
### BITBUCKET

say "Scanning bitbucket"
ssh-keyscan bitbucket.org bitbucket.com > bitbucket.com.keys 2>/dev/null
bitbucket_fingerprint=$( ssh-keygen -l -f bitbucket.com.keys | grep bitbucket.org | head -n1 )

say "Obtaining declared fingerprints from $BITBUCKET_FINGERPRINTS_URL"
set +e
curl -s -o bitbucket_fingerprints.html $BITBUCKET_FINGERPRINTS_URL
bitbucket_sha_lines=$( cat bitbucket_fingerprints.html | \
                        grep -A30 "The public key fingerprints for the Bitbucket server are" \
                        | grep code | grep SHA )

say "Comparing bitbucket fingerprints"
if [ ! -z "$bitbucket_fingerprint" ] && echo "$bitbucket_sha_lines" | grep -q "$bitbucket_fingerprint" ; then
   say "Bitbucket fingerprint validated"
   cat bitbucket.com.keys 
else
   say "ERROR: Validation failed for bitbucket keys!"
fi  
set -e


#########################################
### GITLAB

say "Scanning gitlab.com" 
ssh-keyscan gitlab.com > gitlab.com.keys 2>/dev/null
gitlab_fingerprint=$( ssh-keygen -l -f gitlab.com.keys | head -n1 | cut -f2 -d' ' | cut -f2 -d: )


say "Obtaining announced fingerprints from $GITLAB_FINGERPRINTS_URL"
curl -s -o gitlab_fingerprints.html $GITLAB_FINGERPRINTS_URL

say "Comparing gitlab fingerprints"
#gitlab_sha_lines=$( grep -A30 SHA256 gitlab_fingerprints.html | python -c 'import re; import sys; fingerprint_regexp=r"(?:[0-9a-zA-Z]{2}\:){15}[0-9a-zA-Z]{2}</code><td><code [^>]*>([^<]*)</code>"; lines= re.findall(fingerprint_regexp,sys.stdin.read()) ; print ("\n".join(lines)) ' | tee gitlab_sha_lines.txt  )
gitlab_sha_lines=$( grep -A30 SHA256 gitlab_fingerprints.html | grep '<td><code' | cut -f3 -d'<' | cut -f2 -d '>' )
if [ ! -z "$gitlab_fingerprint" ] && echo "$gitlab_sha_lines" | grep -q "$gitlab_fingerprint" ; then
   say "Gitlab fingerprint validated"
   cat gitlab.com.keys 
else
   say "ERROR: Validation failed for gitlab keys!"
   if [ "$DEBUG" = "yes" ]; then
    say "DEBUG: gitlab_fingerprint=$gitlab_fingerprint"
    say "DEBUG: gitlab_sha_lines=$gitlab_sha_lines"
   fi 
fi  

cd - 2>/dev/null 1>&2
if [ "$DEBUG" = "yes" ]; then
    echo "Not deleting TMPDIR $TMPDIR"
else
    rm -rf $TMPDIR
fi 
